import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ItemsRoutingModule } from './items-routing.module';
import { SharedModule } from '../shared/shared.module';

import { AddItemComponent } from './containers/add-item/add-item.component';
import { ListItemsComponent } from './containers/list-items/list-items.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ItemsRoutingModule
  ],
  declarations: [
    ListItemsComponent,
    AddItemComponent
  ],
  exports: [
    ListItemsComponent,
    AddItemComponent
  ]
})
export class ItemsModule { }
