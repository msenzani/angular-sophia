import { Item } from '../shared/models/item.model';
import { State } from '../shared/enums/state.enum';

export const COLLECTION: Item[] = [
  {
    id: 'a1',
    name: 'Christophe',
    reference: '111',
    state: State.ALIVRER
  },
  {
    id: 'b1',
    name: 'Nadine',
    reference: '222',
    state: State.ENCOURS
  },
  {
    id: 'c1',
    name: 'Marc',
    reference: '333',
    state: State.LIVREE
  }
];
