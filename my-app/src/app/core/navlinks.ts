export const NAVLINKS = {
  home: {
    routerLink: '/home',
    intitule: 'Accueil'
  },
  list : {
    routerLink: '/items/list',
    intitule: 'Liste'
  },
  add: {
    routerLink: '/items/add',
    intitule: 'Ajouter'
  }
};
