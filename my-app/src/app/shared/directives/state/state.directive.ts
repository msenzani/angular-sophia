import { Directive, Input, HostBinding, OnChanges } from '@angular/core';

import { State } from '../../enums/state.enum';

@Directive({
  selector: '[appState]'
})
export class StateDirective implements OnChanges {

  @Input() appState: State;
  @HostBinding('class') nomClass: string;

  constructor() {
  }

  ngOnChanges() {
    // console.log(this.appState);
    this.nomClass = this.formatCssClass(this.appState);
  }

  private removeAccent(chaine: string): string {
    // https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript
    return chaine.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  }

  private formatCssClass(state: string): string {
    return `state-${this.removeAccent(state)
    .toLowerCase()
    .replace(' ', '')}`;
  }

}
